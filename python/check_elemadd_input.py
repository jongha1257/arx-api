import numpy as np

def read_from_binary_file(filename, dtype, shape):
    with open(filename, "rb") as f:
        return np.frombuffer(f.read(), dtype=dtype).reshape(shape)

def verify_arrays_match(arr1, arr2):
    return np.array_equal(arr1, arr2)

def main():
    # Load the arrays from the binary files
    array1 = read_from_binary_file("input1.bin", np.uint8, (-1,))  # -1 indicates unknown shape
    array2 = read_from_binary_file("input2.bin", np.uint8, (-1,))

    # Check if the two input arrays have the same shape
    if array1.shape != array2.shape:
        print("The two input arrays have different shapes!")
        return

    # Add the two arrays and clip the result
    result = array1.astype(np.uint16) + array2.astype(np.uint16)
    result = np.clip(result, 0, 255).astype(np.uint8)

    # Load the golden array
    golden_array = read_from_binary_file("golden.bin", np.uint8, array1.shape)

    # Verify if the computed result matches the golden array
    if verify_arrays_match(result, golden_array):
        print("The computed result matches the golden array!")
    else:
        print("The computed result does NOT match the golden array!")

if __name__ == "__main__":
    main()
