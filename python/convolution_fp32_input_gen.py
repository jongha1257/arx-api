import numpy as np
# import sys

import torch
import torch.nn.functional as F

# set options
MAX_INPUT_ELEMENT = 30.0
MAX_INPUT_DIM_LENGTH = 4
MAX_KERNEL_ELEMENT = 5
MAX_KERNEL_DIM_LENGTH = 5
MAX_BIAS_ELEMENT = 5
SHIFT = 1
STRIDE_SIZE = 1
PADDING_SIZE = 1

def create_random_32bit_input_tensor(max_dimension_length, max_element_size):
    # Define input tensor (batch_size, channels, height, width) with random numbers
    N = np.random.randint(3, max_dimension_length, dtype=np.uint8)
    H = np.random.randint(3, max_dimension_length, dtype=np.uint8)
    W = np.random.randint(3, max_dimension_length, dtype=np.uint8)
    C = np.random.randint(3, max_dimension_length, dtype=np.uint8)
    return N, H, W, C, max_element_size * torch.rand(N, C, H, W, dtype=torch.float32)

def create_random_32bit_kernel_tensor(max_dimension_length, max_element_size, N, H, W, C):
    # Define input tensor (batch_size, channels, height, width) with random numbers
    KN = np.random.randint(1, N - 1, dtype=np.uint8)
    KH = np.random.randint(1, H - 1, dtype=np.uint8)
    KW = np.random.randint(1, W - 1, dtype=np.uint8)
    return KN, KH, KW, C, max_element_size * torch.rand(KN, C, KH, KW, dtype=torch.float32)

def save_32bit_float_array_to_file(filename, data):
    # 파일에 32-bit float 배열을 저장
    data.tofile(filename)

def save_8bit_int_array_to_file(filename, data):
    # 파일에 8-bit 정수 배열을 저장
    data.tofile(filename)

def load_32bit_float_array_from_file(filename):
    # 파일에서 32-bit float 배열을 불러옴
    return torch.from_numpy(np.fromfile(filename, dtype=np.float32))
    

def main():
    # define tensors
    N, H, W, C, input_tensor = create_random_32bit_input_tensor(MAX_INPUT_DIM_LENGTH, MAX_INPUT_ELEMENT)
    KN, KH, KW, C, kernel_tensor = create_random_32bit_kernel_tensor(MAX_KERNEL_DIM_LENGTH, MAX_KERNEL_ELEMENT, N, H, W, C)
    bias_tensor = MAX_BIAS_ELEMENT * torch.rand(KN, dtype=torch.float32)
    out_H = 1 + (H + 2 * PADDING_SIZE - KH) // STRIDE_SIZE
    out_W = 1 + (W + 2 * PADDING_SIZE - KW) // STRIDE_SIZE

    arguments = np.array([N, H, W, C, KN, KH, KW, PADDING_SIZE, STRIDE_SIZE, out_H, out_W], dtype=np.uint8)

    # tensor를 NHWC 형식으로 변환, 그 다음 flatten한 후 binary file로 저장
    save_32bit_float_array_to_file("input.bin", input_tensor.permute(0, 2, 3, 1).reshape(-1).numpy())
    save_32bit_float_array_to_file("kernel.bin", kernel_tensor.permute(0, 2, 3, 1).reshape(-1).numpy())
    save_32bit_float_array_to_file("bias.bin", bias_tensor.numpy())
    save_8bit_int_array_to_file("arguments.bin", arguments)

    # 다시 input을 불러오고 convolution 연산을 위해 4D로 reconstruct한 후 NCHW 형식으로 변환 
    input_tensor = torch.tensor(load_32bit_float_array_from_file("input.bin")).view(N, H, W, C).permute(0, 3, 1, 2)
    kernel_tensor = torch.tensor(load_32bit_float_array_from_file("kernel.bin")).view(KN, KH, KW, C).permute(0, 3, 1, 2)

    # convolution 연산
    output_tensor_fp32 = F.conv2d(input_tensor, kernel_tensor, bias=bias_tensor, stride=STRIDE_SIZE, padding=PADDING_SIZE)
    output_tensor_int8 = torch.round(output_tensor_fp32).to(dtype=torch.uint8)
    save_8bit_int_array_to_file("golden.bin", output_tensor_int8.permute(0, 2, 3, 1).reshape(-1).numpy())

if __name__ == "__main__":
    main()