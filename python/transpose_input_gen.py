import numpy as np
import sys

def create_random_8bit_int_4d_array(dim):
    # 주어진 크기만큼의 8-bit 4차원 정수 배열을 랜덤한 값으로 생성
    return np.random.randint(0, 256, size=(dim[0], dim[1], dim[2], dim[3]), dtype=np.uint8)

def load_8bit_int_array_from_file(filename, dim):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8).reshape(dim[0], dim[1], dim[2], dim[3])

def save_8bit_int_array_to_file(filename, data):
    # 파일에 8-bit 정수 배열을 저장
    data.tofile(filename)

def main():
    if len(sys.argv) != 9:
        dim = [1, 3, 28, 28]
        idx = [0, 2, 3, 1]
    else:
        dim = [int(arg) for arg in sys.argv[1:5]]
        idx = [int(arg) for arg in sys.argv[5:9]]

    # 주어진 길이만큼의 random한 8-bit integer array 생성
    array1 = create_random_8bit_int_4d_array(dim)

    # 배열을 input.bin 파일에 저장
    save_8bit_int_array_to_file("input1.bin", array1)

    # 저장된 배열을 다시 불러옴
    array1 = load_8bit_int_array_from_file("input1.bin", dim)

    # 배열 전치
    result = array1.transpose(idx[0], idx[1], idx[2], idx[3])

    # 결과를 출력 파일에 저장
    save_8bit_int_array_to_file("golden.bin", result)

if __name__ == "__main__":
    main()
