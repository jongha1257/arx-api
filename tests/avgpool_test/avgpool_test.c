//
// Created by User on 2024-01-10.
//
#include "include/API.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    FILE *fp;
    unsigned char *input0;
    long size;
    // read arguments
    if (argc == 2) {
        // argv[1] is the input0 file
        fp = fopen(argv[1], "rb");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        // read binary file and store in memory
        fseek(fp, 0, SEEK_END);
        long fsize = ftell(fp);
        size = fsize;
        fseek(fp, 0, SEEK_SET);
        input0 = malloc(fsize);
        fread(input0, fsize, 1, fp);
        fclose(fp);

    }
    else if (argc != 1) {
        printf("Invalid number of arguments\n");
    }

    else {
        // data size = 1*28*28*3
        size = 1*28*28*3;
        input0 = malloc(size);
 
        // initialize input0 and input1 with random data
        for (int i = 0; i < 1*28*28*3; i++) {
            input0[i] = (char) rand();
        }
    }

    unsigned char pad_size = 1, stride_size = 1;
    unsigned char KH = 3, KW = 3;
    unsigned char N = 1, H = 28, W = 28, C = 3; 
    unsigned char outH = (H + 2 * pad_size - KH) / stride_size + 1;
    unsigned char outW = (W + 2 * pad_size - KW) / stride_size + 1;
    float input0_scale[] = {1.0}; unsigned char input0_offset[] = {0.0};
    float output_scale[] = {1.0}; unsigned char output_offset[] = {0.0};


    // maxpool_i8 input0
    unsigned char *output = malloc(N * outH * outW * C);
    avgpool_i8(input0, input0_scale, input0_offset, output, output_scale, output_offset, N, H, W, C, KH, KW, pad_size, stride_size);

    // write output to file
    fp = fopen("output.bin", "wb");
    fwrite(output, N * outH * outW * C, 1, fp);
    fclose(fp);

    return 0;
}

