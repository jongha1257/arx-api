add_executable(arx_splat_test
        splat_test.c)

add_custom_command(
        TARGET arx_splat_test POST_BUILD
        COMMAND python3 ${CMAKE_HOME_DIRECTORY}/python/splat_input_gen.py 1024 100
)

target_link_libraries(arx_splat_test PUBLIC arx_api)

add_arxapi_test(NAME arx_splat_test COMMAND ${CMAKE_CURRENT_BINARY_DIR}/arx_splat_test USE_SH 1 PARAMS "${CMAKE_CURRENT_BINARY_DIR}/input1.bin"
        USE_DIFF 1 DIFF_TARGET ${CMAKE_CURRENT_BINARY_DIR}/golden.bin)
