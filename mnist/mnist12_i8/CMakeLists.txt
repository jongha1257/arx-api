# 실행 파일 추가
add_executable(mnist12_i8_test 5_mnist12_i8_test.c)

add_custom_command(
        TARGET mnist12_i8_test POST_BUILD
        COMMAND python3 ${CMAKE_HOME_DIRECTORY}/mnist/mnist12_i8/1_mnist_model_gen.py &&
                python3 ${CMAKE_HOME_DIRECTORY}/mnist/mnist12_i8/2_mnist_dataset_gen.py &&
                python3 ${CMAKE_HOME_DIRECTORY}/mnist/mnist12_i8/3_mnist12_i8_param_gen.py &&
                python3 ${CMAKE_HOME_DIRECTORY}/mnist/mnist12_i8/4_mnist12_i8_onnxruntime.py
)

target_link_libraries(mnist12_i8_test PUBLIC arx_api)

add_arxapi_test(NAME mnist12_i8_test COMMAND ${CMAKE_CURRENT_BINARY_DIR}/mnist12_i8_test USE_SH 1 PARAMS "${CMAKE_CURRENT_BINARY_DIR}/param"
        USE_DIFF 1 DIFF_TARGET ${CMAKE_CURRENT_BINARY_DIR}/golden.bin)

